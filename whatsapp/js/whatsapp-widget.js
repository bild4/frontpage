let paramsDefault = {
    }

function initWidget(params)  {

    document.addEventListener("DOMContentLoaded", function(e) {

        const wpp = document.createElement('div')
        wpp.setAttribute("id", "whatsapp-widget")
        wpp.classList.add('whatsapp-widget')

        const body = document.querySelector('body')
        body.appendChild(wpp)

        createElements(params)
    })
}

function createElements(params) {
    const mainWhatsappWidget  = document.querySelector('#whatsapp-widget')

    if (params) 
        paramsDefault = params

    const strTarget = ` <a href="https://api.whatsapp.com/send?phone=+966560646713&text=Hello, I'm interested in bildnw's services" target="_blank" id="whatsapp-widget-target" class="whatsapp-widget-target pulse">
                            <img id="whatsapp-widget-icon" class="whatsapp-widget-icon" src="https://imagepng.org/wp-content/uploads/2017/08/WhatsApp-icone.png"/>
                        </a>`

    mainWhatsappWidget.innerHTML = strTarget 

}
